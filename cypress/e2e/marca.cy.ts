describe('Pantalla de creación con cancelación', () => {
  it('passes', () => {
    cy.visit('localhost:4200')
    cy.get('.row > [routerlink="/marca"]').click()
    cy.get('.col-2 > .btn').click()
    cy.get(':nth-child(1) > div > .form-control').click().type('Marca de test')
    cy.get(':nth-child(2) > div > .form-control').click().type('1990')
    cy.get(':nth-child(3) > div > .form-control').click().type('España')
    cy.get(':nth-child(4) > div > .form-control').click().type('Descripcion')
    cy.get('.btn-success').should('be.enabled')
    cy.get('.btn-dark').click()
    cy.get('.modal-content').should('be.visible')
    cy.get('.btn-outline-primary').click()
    cy.get('.ng-trigger').should('be.visible').should('contain.text', 'Saliendo sin guardar')
  })
})

describe('Pantalla de creación con confirmación', () => {
  it('passes', () => {
    cy.visit('localhost:4200')
    cy.get('.row > [routerlink="/marca"]').click()
    cy.get('.col-2 > .btn').click()
    cy.get(':nth-child(1) > div > .form-control').click().type('Marca de test')
    cy.get(':nth-child(2) > div > .form-control').click().type('1990')
    cy.get(':nth-child(3) > div > .form-control').click().type('España')
    cy.get(':nth-child(4) > div > .form-control').click().type('Descripcion')
    cy.get('.btn-success').should('be.enabled').click()
    cy.get('.modal-content').should('be.visible')
    cy.get('.btn-outline-primary').click()
    cy.get('.ng-trigger').should('be.visible').should('contain.text', 'Creado con éxito')
  })
})

describe('Pantalla de eliminar', () => {
  it('passes', () => {
    cy.visit('localhost:4200')
    cy.get('.row > [routerlink="/marca"]').click()
    cy.get(':nth-child(3) > .page-link').click()
    cy.get(':nth-child(2) > [style="min-width: 120px;"] > .btn-outline-danger').should('be.enabled').click()
    cy.get(':nth-child(2)').should('be.enabled')

  })
})