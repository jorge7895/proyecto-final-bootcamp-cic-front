describe('Pantalla de creación', () => {
  it('passes', () => {
    cy.visit('localhost:4200')
    cy.get('.row > [routerlink="/tipo"]').click()
    cy.get('.col-2 > .btn').click()
    cy.get(':nth-child(1) > div > .form-control').click().type('T')
    cy.get(':nth-child(2) > div > .form-control').click()
    cy.get(':nth-child(1) > div > .form-text').should('be.visible').should('contain.text', 'Este campo es obligatorio')
    cy.get(':nth-child(2) > div > .form-control').click().type('Descripción de test')
    cy.get('.btn-dark').should('be.enabled').click()
    cy.get('.modal-content').should('be.visible')
    cy.get('.btn-outline-primary').click()
    cy.get('.ng-trigger').should('be.visible').should('contain.text', 'Saliendo sin guardar')
  })
})