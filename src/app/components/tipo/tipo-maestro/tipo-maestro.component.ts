import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Tipo } from 'src/app/models/tipo';
import { TipoService } from 'src/app/services/tipo.service';

@Component({
  selector: 'app-tipo-maestro',
  templateUrl: './tipo-maestro.component.html',
  styleUrls: ['./tipo-maestro.component.css']
})
export class TipoMaestroComponent implements OnInit {

  listadoDeTipos: Tipo[] = [];

  page:number=1;
  numeroDePaginas:number = 5;
  resultadosPagina:number=9


  constructor(
    private tipoService: TipoService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.tipoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => this.numeroDePaginas = res)
    this.cargarTiposDelServidor();

  }

  cargarTiposDelServidor(){

    this.listadoDeTipos = [];

    this.tipoService.leerTodosLosTipos(this.page-1,this.resultadosPagina).subscribe(tiposObtenidos => {
      this.listadoDeTipos = tiposObtenidos,
      console.log(this.listadoDeTipos)
    });
    
  }

  verDetalleDeTipo(id: number){

    this.router.navigate(["tipo", id]);

  }

  crearNuevoTipo(){

    this.router.navigate(["tipo", 0]);

  }

  eliminarTipo(id: number){
    
    this.tipoService.eliminarUnTipo(id).subscribe(_ => this.cargarTiposDelServidor());
  }
}
