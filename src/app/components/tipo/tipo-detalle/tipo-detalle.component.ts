import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Tipo } from 'src/app/models/tipo';
import { Vehiculo } from 'src/app/models/vehiculo';
import { TipoService } from 'src/app/services/tipo.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { ModalAlertaComponent } from '../../modales/modal-alerta/modal-alerta.component';

@Component({
  selector: 'app-tipo-detalle',
  templateUrl: './tipo-detalle.component.html',
  styleUrls: ['./tipo-detalle.component.css']
})
export class TipoDetalleComponent implements OnInit {

  tipoFormulario?: FormGroup;
  idTipoActual: number = 0;
  tipoActual?: Tipo;
  inactivo: boolean = true;
  listadoDeVehiculos: Vehiculo[] = [];

  page: number = 1;
  numeroDePaginas: number = 5;
  resultadosPagina: number = 9


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private tipoService: TipoService,
    private vehiculoService: VehiculoService,
    private modalService: NgbModal,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {

    this.vehiculoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => {
      this.numeroDePaginas = res;;
    })

    this.idTipoActual = parseInt(this.route.snapshot.paramMap.get('id')!);

    this.cargarFormulario(this.idTipoActual);
  }

  cargarVehiculos(nombreTipo: string) {

    this.vehiculoService.leerVehiculoPorTipo(this.page - 1, this.resultadosPagina, nombreTipo).subscribe(v => this.listadoDeVehiculos = v);
  }

  cargarFormulario(id: number) {

    this.idTipoActual = id;

    if (this.inactivo && id !== 0) {

      this.tipoService.leerTipoPorId(id).subscribe(t => {
        this.tipoActual = t;
        this.tipoFormulario = this.formBuilder.group({
          nombre: [{ value: this.tipoActual.nombre, disabled: this.inactivo }, [Validators.required]],
          descripcion: [{ value: this.tipoActual.descripcion, disabled: this.inactivo }]
        })
        this.cargarVehiculos(t.nombre);
      });

    } else if (id === 0) {

      this.tipoFormulario = this.formBuilder.group({
        nombre: ['', [Validators.required, Validators.minLength(5)]],
        descripcion: ['', [Validators.required, Validators.minLength(5)]],
      })
      this.inactivo = false;

    } else if (!this.inactivo && id !== 0) {

      this.tipoService.leerTipoPorId(id).subscribe(t => {
        this.tipoActual = t;
        this.tipoFormulario = this.formBuilder.group({
          nombre: [{ value: this.tipoActual.nombre, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
          descripcion: [{ value: this.tipoActual.descripcion, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
        })
        this.cargarVehiculos(t.nombre);
      });
    }
  }

  guardar() {
    let modalRef = this.modalService.open(ModalAlertaComponent);
    modalRef.componentInstance.mensaje = "¿Está seguro que quieres guardar este tipo?";
    modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
      if (res) {
        if (!this.inactivo && this.idTipoActual !== 0) {

          let tipo: Tipo = {
            id: this.idTipoActual,
            nombre: this.tipoFormulario!.value.nombre,
            descripcion: this.tipoFormulario!.value.descripcion,
          }
          this.tipoService.actualizarTipo(this.idTipoActual, tipo).subscribe(res => {
            this.router.navigate(["/tipo/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;

            this.toastr.success('Editado con éxito');
          });

        } else {

          let tipo: Tipo = {
            id: 0,
            nombre: this.tipoFormulario!.value.nombre,
            descripcion: this.tipoFormulario!.value.descripcion,
          }
          this.tipoService.crearTipoNuevo(tipo).subscribe(res => {
            this.router.navigate(["/tipo/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Creado con éxito');
          });
        }
      }
    });
  }

  actualizar() {

    this.inactivo = false;
    if (!this.inactivo) {

      this.tipoFormulario?.get('nombre')?.enable();
      this.tipoFormulario?.get('descripcion')?.enable();
    }
    this.cargarFormulario(this.idTipoActual);

  }

  volver() {

    if(!this.inactivo){
      let modalRef = this.modalService.open(ModalAlertaComponent);
      modalRef.componentInstance.mensaje = "¿Estás seguro de que quieres volver atras?";
      modalRef.componentInstance.mensaje2 = "Se perderán los cambios no guardados";
      modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
        if (res) {
          this.router.navigate(["/tipo"]);
          this.toastr.warning("Saliendo sin guardar")
        }
      });
    }else{
      this.router.navigate(["/tipo"]);
    }


  }

  campoNoEsValido(campo: string) {
    return this.tipoFormulario!.controls?.[campo].errors
      && this.tipoFormulario!.controls?.[campo].touched
  }
}
