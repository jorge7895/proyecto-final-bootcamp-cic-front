import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Grupo } from 'src/app/models/grupo';
import { Marca } from 'src/app/models/marca';
import { Tipo } from 'src/app/models/tipo';
import { TipoCombustible } from 'src/app/models/tipo-combustible';
import { TipoTraccion } from 'src/app/models/tipo-traccion';
import { Vehiculo } from 'src/app/models/vehiculo';
import { GrupoVehiculoService } from 'src/app/services/grupo-vehiculo.service';
import { MarcaService } from 'src/app/services/marca.service';
import { TipoService } from 'src/app/services/tipo.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { __values } from 'tslib';
import { ModalAlertaComponent } from '../../modales/modal-alerta/modal-alerta.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vehiculo-detalle',
  templateUrl: './vehiculo-detalle.component.html',
  styleUrls: ['./vehiculo-detalle.component.css']
})
export class VehiculoDetalleComponent implements OnInit {

  vehiculoFormulario?: FormGroup;
  idVehiculoActual: number = 0;
  vehiculoActual?: Vehiculo;
  inactivo: boolean = true;
  listadoDeVehiculos: Vehiculo[] = [];
  marcaActual?: Marca;
  tipoActual?: Tipo;
  listadoDeTipos: Tipo[] = [];
  listadoDeMarcas: Marca[] = [];
  listadoDeCombustibles = Object.values(TipoCombustible);
  listadoDeTracciones = Object.values(TipoTraccion)
  listadoDeGrupos: Grupo[] = [];
  page: number = 1;
  numeroDePaginas: number = 5;
  resultadosPagina: number = 9


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private vehiculoService: VehiculoService,
    private tipoService: TipoService,
    private marcaService: MarcaService,
    private grupoVehiculoService: GrupoVehiculoService,
    private modalService: NgbModal,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {

    this.vehiculoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => {
      this.numeroDePaginas = res;;
    })

    this.idVehiculoActual = parseInt(this.route.snapshot.paramMap.get('id')!);

    this.cargarFormulario(this.idVehiculoActual);
  }

  cargarGrupos(id: number) {
    this.listadoDeGrupos = [];
    this.grupoVehiculoService.leerGruposDeUnVehiculo(0, 10, id).subscribe(grupo => this.listadoDeGrupos = grupo);
  }

  cargarVehiculos() {

    this.vehiculoService.leerTodosLosVehiculos(this.page - 1, this.resultadosPagina).subscribe(v => this.listadoDeVehiculos = v);
  }

  cargarFormulario(id: number) {

    this.idVehiculoActual = id;


    if (this.inactivo && id !== 0) {

      this.vehiculoService.leerVehiculoPorId(id).subscribe(vehiculo => {

        this.vehiculoActual = vehiculo;
        this.cargarGrupos(vehiculo.id);
        this.tipoService.leerTodosLosTipos(0, 20).subscribe(tp => {
          console.log(vehiculo);

          this.listadoDeTipos = tp
          this.tipoActual = tp.find(tipo => tipo.id == vehiculo.idTipo)
          console.log(this.tipoActual);
        });

        this.marcaService.leerTodosLosMarcas(0, 20).subscribe(m => {
          this.listadoDeMarcas = m
          this.marcaActual = m.find(marca => marca.id == vehiculo.idMarca)
          console.log(this.marcaActual);

        });
        this.vehiculoFormulario = this.formBuilder.group({
          idMarca: [{ value: this.vehiculoActual.idMarca, disabled: this.inactivo }],
          descripcion: [{ value: this.vehiculoActual.descripcion, disabled: this.inactivo }],
          idTipo: [{ value: this.vehiculoActual.idTipo, disabled: this.inactivo }],
          modelo: [{ value: this.vehiculoActual.modelo, disabled: this.inactivo }],
          matricula: [{ value: this.vehiculoActual.matricula, disabled: this.inactivo }],
          kilometros: [{ value: this.vehiculoActual.kilometros, disabled: this.inactivo }],
          anoFabricacion: [{ value: this.vehiculoActual.anoFabricacion, disabled: this.inactivo }],
          puertas: [{ value: this.vehiculoActual.puertas, disabled: this.inactivo }],
          plazas: [{ value: this.vehiculoActual.plazas, disabled: this.inactivo }],
          traccion: [{ value: this.vehiculoActual.traccion, disabled: this.inactivo }],
          combustible: [{ value: this.vehiculoActual.combustible, disabled: this.inactivo }],
        })

        this.cargarVehiculos();
      });

    } else if (id === 0) {

      this.tipoService.leerTodosLosTipos(0, 20).subscribe(tp => {
        this.listadoDeTipos = tp
      });

      this.marcaService.leerTodosLosMarcas(0, 20).subscribe(m => {
        this.listadoDeMarcas = m

      });
      this.vehiculoFormulario = this.formBuilder.group({
        descripcion: ['', [Validators.required],[Validators.minLength(5)]],
        idMarca: ['', [Validators.required]],
        idTipo: ['', [Validators.required]],
        modelo: ['', [Validators.required]],
        matricula: ['', [Validators.required]],
        kilometros: ['', [Validators.required]],
        anoFabricacion: ['', [Validators.required]],
        puertas: ['', [Validators.required]],
        plazas: ['', [Validators.required]],
        traccion: ['', [Validators.required]],
        combustible: ['', [Validators.required]]
      })
      this.inactivo = false;

    } else if (!this.inactivo && id !== 0) {

      this.vehiculoService.leerVehiculoPorId(id).subscribe(t => {
        this.vehiculoActual = t;
        this.vehiculoFormulario = this.formBuilder.group({

          idMarca: [{ value: this.vehiculoActual.idMarca, disabled: this.inactivo }, [Validators.required]],
          descripcion: [{ value: this.vehiculoActual.descripcion, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
          idTipo: [{ value: this.vehiculoActual.idTipo, disabled: this.inactivo }, [Validators.required]],
          modelo: [{ value: this.vehiculoActual.modelo, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
          matricula: [{ value: this.vehiculoActual.matricula, disabled: this.inactivo }, [Validators.required, Validators.minLength(7), Validators.maxLength(7)]],
          kilometros: [{ value: this.vehiculoActual.kilometros, disabled: this.inactivo }, [Validators.required]],
          anoFabricacion: [{ value: this.vehiculoActual.anoFabricacion, disabled: this.inactivo }, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
          puertas: [{ value: this.vehiculoActual.puertas, disabled: this.inactivo }, [Validators.required, Validators.minLength(1), Validators.maxLength(1)]],
          plazas: [{ value: this.vehiculoActual.plazas, disabled: this.inactivo }, [Validators.required]],
          traccion: [{ value: this.vehiculoActual.traccion, disabled: this.inactivo }, [Validators.required]],
          combustible: [{ value: this.vehiculoActual.combustible, disabled: this.inactivo }, [Validators.required]],
        })
        this.cargarVehiculos();
        this.cargarGrupos(t.id);
      });
    }
  }

  guardar() {
    let modalRef = this.modalService.open(ModalAlertaComponent);
    modalRef.componentInstance.mensaje = "¿Está seguro que quieres guardar este vehículo?";
    modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
      if (res) {
        if (!this.inactivo && this.idVehiculoActual !== 0) {

          let marcaNuev = this.listadoDeMarcas.find(marca => marca.id == this.vehiculoFormulario!.value.idMarca.id);
          if (!marcaNuev) {
            marcaNuev = this.marcaActual
          }

          let tipoNuev = this.listadoDeTipos.find(tipo => tipo.id == this.vehiculoFormulario!.value.idTipo.id);
          if (!tipoNuev) {
            tipoNuev = this.tipoActual
          }

          let vehiculo: Vehiculo = {
            id: this.idVehiculoActual,
            idMarca: this.vehiculoFormulario!.value.idMarca,
            marca: marcaNuev,
            descripcion: this.vehiculoFormulario!.value.descripcion,
            idTipo: this.vehiculoFormulario!.value.idTipo,
            tipo: tipoNuev,
            modelo: this.vehiculoFormulario!.value.modelo,
            matricula: this.vehiculoFormulario!.value.matricula,
            kilometros: this.vehiculoFormulario!.value.kilometros,
            anoFabricacion: this.vehiculoFormulario!.value.anoFabricacion,
            puertas: this.vehiculoFormulario!.value.puertas,
            plazas: this.vehiculoFormulario!.value.plazas,
            traccion: this.vehiculoFormulario!.value.traccion.toUpperCase(),
            combustible: this.vehiculoFormulario!.value.combustible.toUpperCase()
          }
          this.vehiculoService.actualizarVehiculo(this.idVehiculoActual, vehiculo).subscribe(res => {
            this.router.navigate(["/vehiculo/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Editado con éxito');
          });

        } else {

          let vehiculo: Vehiculo = {
            id: 0,
            idMarca: this.vehiculoFormulario!.value.idMarca,
            marca: this.listadoDeMarcas.find(marca => marca.id == this.vehiculoFormulario!.value.idMarca.id),
            descripcion: this.vehiculoFormulario!.value.descripcion,
            idTipo: this.vehiculoFormulario!.value.idTipo,
            tipo: this.listadoDeTipos.find(tipo => tipo.id == this.vehiculoFormulario!.value.idTipo.id),
            modelo: this.vehiculoFormulario!.value.modelo,
            matricula: this.vehiculoFormulario!.value.matricula,
            kilometros: this.vehiculoFormulario!.value.kilometros,
            anoFabricacion: this.vehiculoFormulario!.value.anoFabricacion,
            puertas: this.vehiculoFormulario!.value.puertas,
            plazas: this.vehiculoFormulario!.value.plazas,
            traccion: this.vehiculoFormulario!.value.traccion.toUpperCase(),
            combustible: this.vehiculoFormulario!.value.combustible.toUpperCase()
          }
          this.vehiculoService.crearVehiculoNuevo(vehiculo).subscribe(res => {
            this.router.navigate(["/vehiculo/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Creado con éxito');
          });
        }
      }
    });
  }

  actualizar() {

    this.inactivo = false;
    if (!this.inactivo) {

      this.vehiculoFormulario?.get('modelo')?.enable();
      this.vehiculoFormulario?.get('descripcion')?.enable();
      this.vehiculoFormulario?.get('idMarca')?.enable();
      this.vehiculoFormulario?.get('idTipo')?.enable();
      this.vehiculoFormulario?.get('matricula')?.enable();
      this.vehiculoFormulario?.get('kilometros')?.enable();
      this.vehiculoFormulario?.get('combustible')?.enable();
      this.vehiculoFormulario?.get('anoFabricacion')?.enable();
      this.vehiculoFormulario?.get('puertas')?.enable();
      this.vehiculoFormulario?.get('traccion')?.enable();
      this.vehiculoFormulario?.get('plazas')?.enable();
    }
    this.cargarFormulario(this.idVehiculoActual);

  }

  volver() {
    if (!this.inactivo) {
      let modalRef = this.modalService.open(ModalAlertaComponent);
      modalRef.componentInstance.mensaje = "¿Estás seguro de que quieres volver atras?";
      modalRef.componentInstance.mensaje2 = "¡Se perderán los cambios no guardados!";
      modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
        if (res) {
          this.router.navigate(["/vehiculo"]);
          this.toastr.warning("Saliendo sin guardar")
        }
      });
    } else {
      this.router.navigate(["/vehiculo"]);
    }


  }

  campoNoEsValido(campo: string) {
    return this.vehiculoFormulario!.controls?.[campo].errors
      && this.vehiculoFormulario!.controls?.[campo].touched
  }

}
