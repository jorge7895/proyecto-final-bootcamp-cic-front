import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Vehiculo } from 'src/app/models/vehiculo';
import { VehiculoService } from 'src/app/services/vehiculo.service';

@Component({
  selector: 'app-vehiculo-maestro',
  templateUrl: './vehiculo-maestro.component.html',
  styleUrls: ['./vehiculo-maestro.component.css']
})
export class VehiculoMaestroComponent implements OnInit {

  listadoDeVehiculos: Vehiculo[] = [];

  page:number=1;
  numeroDePaginas:number = 5;
  resultadosPagina:number=9


  constructor(
    private vehiculoService: VehiculoService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.vehiculoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => this.numeroDePaginas = res)
    this.cargarVehiculosDelServidor();

  }

  cargarVehiculosDelServidor(){

    this.listadoDeVehiculos = [];

    this.vehiculoService.leerTodosLosVehiculos(this.page-1,this.resultadosPagina).subscribe(vehiculosObtenidos => {
      this.listadoDeVehiculos = vehiculosObtenidos,
      console.log(this.listadoDeVehiculos)
    });
    
  }

  verDetalleDeVehiculo(id: number){

    this.router.navigate(["vehiculo", id]);

  }

  crearNuevoVehiculo(){

    this.router.navigate(["vehiculo", 0]);

  }

  eliminarVehiculo(id: number){
    
    this.vehiculoService.eliminarUnVehiculo(id).subscribe(_ => this.cargarVehiculosDelServidor());
  }

}
