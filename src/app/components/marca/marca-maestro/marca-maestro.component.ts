import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Marca } from 'src/app/models/marca';
import { MarcaService } from 'src/app/services/marca.service';

@Component({
  selector: 'app-marca-maestro',
  templateUrl: './marca-maestro.component.html',
  styleUrls: ['./marca-maestro.component.css']
})
export class MarcaMaestroComponent implements OnInit {

  listadoDeMarcas: Marca[] = [];

  page:number=1;
  sizePage:number = 5;
  resultadosPagina:number=9

  constructor(
    private marcaService: MarcaService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.marcaService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => this.sizePage = res)
    this.cargarMarcasDelServidor();

  }

  cargarMarcasDelServidor(){

    this.listadoDeMarcas = [];

    this.marcaService.leerTodosLosMarcas(this.page-1,this.resultadosPagina).subscribe(MarcasObtenidos => {
      this.listadoDeMarcas = MarcasObtenidos,
      console.log(this.listadoDeMarcas)
    });
    
  }

  verDetalleDeMarca(id: number){

    this.router.navigate(["marca", id]);

  }

  crearNuevoMarca(){

    this.router.navigate(["marca", 0]);

  }

  eliminarMarca(id: number){
    
    this.marcaService.eliminarUnMarca(id).subscribe(_ => this.cargarMarcasDelServidor());
  }

}
