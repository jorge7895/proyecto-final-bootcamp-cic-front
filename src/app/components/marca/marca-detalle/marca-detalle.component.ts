import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Marca } from 'src/app/models/marca';
import { Vehiculo } from 'src/app/models/vehiculo';
import { MarcaService } from 'src/app/services/marca.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { ModalAlertaComponent } from '../../modales/modal-alerta/modal-alerta.component';

@Component({
  selector: 'app-marca-detalle',
  templateUrl: './marca-detalle.component.html',
  styleUrls: ['./marca-detalle.component.css']
})
export class MarcaDetalleComponent implements OnInit {

  marcaFormulario?: FormGroup;
  idMarcaActual: number = 0;
  marcaActual?: Marca;
  inactivo: boolean = true;
  listadoDeVehiculos: Vehiculo[] = [];

  page: number = 1;
  numeroDePaginas: number = 5;
  resultadosPagina: number = 9

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private marcaService: MarcaService,
    private vehiculoService: VehiculoService,
    private modalService: NgbModal,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {

    this.vehiculoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => {
      this.numeroDePaginas = res;;
    })

    this.idMarcaActual = parseInt(this.route.snapshot.paramMap.get('id')!);

    this.cargarFormulario(this.idMarcaActual);
  }

  cargarVehiculos(nombreMarca: string) {

    this.vehiculoService.leerVehiculoPorMarca(this.page - 1, this.resultadosPagina, nombreMarca).subscribe(v => this.listadoDeVehiculos = v);
  }


  cargarFormulario(id: number) {

    this.idMarcaActual = id;

    if (this.inactivo && id !== 0) {

      this.marcaService.leerMarcaPorId(id).subscribe(t => {
        this.marcaActual = t;
        this.marcaFormulario = this.formBuilder.group({
          nombre: [{ value: this.marcaActual.nombre, disabled: this.inactivo }],
          descripcion: [{ value: this.marcaActual.descripcion, disabled: this.inactivo }],
          anoCreacion: [{ value: this.marcaActual.anoCreacion, disabled: this.inactivo }],
          sede: [{ value: this.marcaActual.sede, disabled: this.inactivo }],
        })
        this.cargarVehiculos(t.nombre);
      });

    } else if (id === 0) {

      this.marcaFormulario = this.formBuilder.group({
        nombre: ['', [Validators.required, Validators.minLength(5)]],
        descripcion: ['', [Validators.required, Validators.minLength(5)]],
        anoCreacion: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
        sede: ['', [Validators.required, Validators.minLength(5)]],
      })
      this.inactivo = false;

    } else if (!this.inactivo && id !== 0) {

      this.marcaService.leerMarcaPorId(id).subscribe(t => {
        this.marcaActual = t;
        this.marcaFormulario = this.formBuilder.group({
          nombre: [{ value: this.marcaActual.nombre, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
          descripcion: [{ value: this.marcaActual.descripcion, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
          anoCreacion: [{ value: this.marcaActual.anoCreacion, disabled: this.inactivo }, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
          sede: [{ value: this.marcaActual.sede, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
        })
        this.cargarVehiculos(t.nombre);
      });
    }
  }

  guardar() {
    let modalRef = this.modalService.open(ModalAlertaComponent);
    modalRef.componentInstance.mensaje = "¿Está seguro que quieres guardar esta marca?";
    modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
      if (res) {
        if (!this.inactivo && this.idMarcaActual !== 0) {

          let marca: Marca = {
            id: this.idMarcaActual,
            nombre: this.marcaFormulario!.value.nombre,
            descripcion: this.marcaFormulario!.value.descripcion,
            anoCreacion: this.marcaFormulario!.value.anoCreacion,
            sede: this.marcaFormulario!.value.sede
          }
          this.marcaService.actualizarMarca(this.idMarcaActual, marca).subscribe(res => {
            this.router.navigate(["/marca/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Editado con éxito');
          });

        } else {

          let marca: Marca = {
            id: 0,
            nombre: this.marcaFormulario!.value.nombre,
            descripcion: this.marcaFormulario!.value.descripcion,
            anoCreacion: this.marcaFormulario!.value.anoCreacion,
            sede: this.marcaFormulario!.value.sede
          }
          this.marcaService.crearMarcaNuevo(marca).subscribe(res => {
            this.router.navigate(["/marca/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Creado con éxito');
          });
        }
      }
    });
  }

  actualizar() {

    this.inactivo = false;
    if (!this.inactivo) {

      this.marcaFormulario?.get('nombre')?.enable();
      this.marcaFormulario?.get('descripcion')?.enable();
      this.marcaFormulario?.get('anoCreacion')?.enable();
      this.marcaFormulario?.get('sede')?.enable();
    }
    this.cargarFormulario(this.idMarcaActual);

  }

  volver() {
    if (!this.inactivo) {
      let modalRef = this.modalService.open(ModalAlertaComponent);
      modalRef.componentInstance.mensaje = "¿Estás seguro de que quieres volver atras?";
      modalRef.componentInstance.mensaje2 = "¡Se perderán los cambios no guardados!";
      modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
        if (res) {
          this.router.navigate(["/marca"]);
          this.toastr.warning("Saliendo sin guardar")
        }
      });
    } else {
      this.router.navigate(["/marca"]);
    }
  }

  campoNoEsValido(campo: string) {
    return this.marcaFormulario!.controls?.[campo].errors
      && this.marcaFormulario!.controls?.[campo].touched
  }
}
