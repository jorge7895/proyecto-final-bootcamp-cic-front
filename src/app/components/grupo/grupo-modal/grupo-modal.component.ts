import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Vehiculo } from 'src/app/models/vehiculo';

@Component({
  selector: 'app-grupo-modal',
  templateUrl: './grupo-modal.component.html',
  styleUrls: ['./grupo-modal.component.css']
})
export class GrupoModalComponent {

  @Input() vehiculosDisponibles?: Vehiculo[];
  @Output() emisorVehiculo: EventEmitter<Vehiculo> = new EventEmitter();
  selectedVehiculo?: Vehiculo;

  constructor(public activeModal: NgbActiveModal) {}

  guardar(){
    this.emisorVehiculo.emit(this.selectedVehiculo);
    this.activeModal.close();
  }

}
