import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Grupo } from 'src/app/models/grupo';
import { GrupoService } from 'src/app/services/grupo.service';

@Component({
  selector: 'app-grupo-maestro',
  templateUrl: './grupo-maestro.component.html',
  styleUrls: ['./grupo-maestro.component.css']
})
export class GrupoMaestroComponent implements OnInit {

  listadoDeGrupos: Grupo[] = [];

  page:number=1;
  sizePage:number = 5;
  resultadosPagina:number=9

  constructor(
    private grupoService: GrupoService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.grupoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => this.sizePage = res)
    this.cargarGruposDelServidor();

  }

  cargarGruposDelServidor(){

    this.listadoDeGrupos = [];

    this.grupoService.leerTodosLosGrupos(this.page-1,this.resultadosPagina).subscribe(GruposObtenidos => {
      this.listadoDeGrupos = GruposObtenidos,
      console.log(this.listadoDeGrupos)
    });
    
  }

  verDetalleDeGrupo(id: number){

    this.router.navigate(["grupo", id]);

  }

  crearNuevoGrupo(){

    this.router.navigate(["grupo", 0]);

  }

  eliminarGrupo(id: number){
    
    this.grupoService.eliminarUnGrupo(id).subscribe(_ => this.cargarGruposDelServidor());
  }

}
