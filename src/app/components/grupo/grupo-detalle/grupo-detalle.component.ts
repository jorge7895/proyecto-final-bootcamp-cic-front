import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Grupo } from 'src/app/models/grupo';
import { GrupoVehiculo } from 'src/app/models/grupovehiculo';
import { Vehiculo } from 'src/app/models/vehiculo';
import { GrupoVehiculoService } from 'src/app/services/grupo-vehiculo.service';
import { GrupoService } from 'src/app/services/grupo.service';
import { VehiculoService } from 'src/app/services/vehiculo.service';
import { ModalAlertaComponent } from '../../modales/modal-alerta/modal-alerta.component';
import { GrupoModalComponent } from '../grupo-modal/grupo-modal.component';

@Component({
  selector: 'app-grupo-detalle',
  templateUrl: './grupo-detalle.component.html',
  styleUrls: ['./grupo-detalle.component.css']
})
export class GrupoDetalleComponent implements OnInit {

  listaGrupoVehiculo: GrupoVehiculo[] = [];
  grupoFormulario?: FormGroup;
  idGrupoActual: number = 0;
  grupoActual?: Grupo;
  inactivo: boolean = true;
  listadoDeVehiculos: Vehiculo[] = [];
  nuevaListaGrupoVehiculo: GrupoVehiculo[] = [];

  page: number = 1;
  numeroDePaginas: number = 5;
  resultadosPagina: number = 9

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private grupoService: GrupoService,
    private vehiculoService: VehiculoService,
    private grupoVehiculoService: GrupoVehiculoService,
    private modalService: NgbModal,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {

    this.vehiculoService.leerNumeroDePaginas(this.resultadosPagina).subscribe(res => this.numeroDePaginas = res)

    this.idGrupoActual = parseInt(this.route.snapshot.paramMap.get('id')!);

    this.cargarFormulario(this.idGrupoActual);
  }

  cargarVehiculos(id: number) {
    this.listadoDeVehiculos = [];
    this.grupoVehiculoService.leerVehiculosDeUnGrupo(this.page - 1, this.resultadosPagina, id).subscribe(v => this.listadoDeVehiculos = v);
  }

  cargarGrupoVehiculo() {
    this.listaGrupoVehiculo = [];
    this.grupoVehiculoService.leerGruposVehiculosPorGrupo(this.idGrupoActual).subscribe(gv => {
      this.listaGrupoVehiculo = gv;
      this.nuevaListaGrupoVehiculo = gv

      console.log(this.nuevaListaGrupoVehiculo);
    })
  }

  cargarFormulario(id: number) {

    this.idGrupoActual = id;

    if (this.inactivo && id !== 0) {

      this.grupoService.leerGrupoPorId(id).subscribe(t => {
        this.grupoActual = t;
        this.grupoFormulario = this.formBuilder.group({
          nombre: [{ value: this.grupoActual.nombre, disabled: this.inactivo }],
          descripcion: [{ value: this.grupoActual.descripcion, disabled: this.inactivo }],
        })
        this.cargarVehiculos(t.id);
      });

    } else if (id === 0) {

      this.grupoFormulario = this.formBuilder.group({
        nombre: ['', [Validators.required, Validators.minLength(5)]],
        descripcion: ['', [Validators.required, Validators.minLength(20)]],
      })
      this.inactivo = false;

    } else if (!this.inactivo && id !== 0) {

      this.grupoService.leerGrupoPorId(id).subscribe(t => {
        this.grupoActual = t;
        this.grupoFormulario = this.formBuilder.group({
          nombre: [{ value: this.grupoActual.nombre, disabled: this.inactivo }, [Validators.required, Validators.minLength(5)]],
          descripcion: [{ value: this.grupoActual.descripcion, disabled: this.inactivo }, [Validators.required, Validators.minLength(20)]],
        })
        this.cargarVehiculos(t.id);
      });
    }
  }

  guardar() {
    let modalRef = this.modalService.open(ModalAlertaComponent);
    modalRef.componentInstance.mensaje = "¿Está seguro que quieres guardar este vehículo?";
    modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
      if (res) {
        if (!this.inactivo && this.idGrupoActual !== 0) {

          let grupo: Grupo = {
            id: this.idGrupoActual,
            nombre: this.grupoFormulario!.value.nombre,
            descripcion: this.grupoFormulario!.value.descripcion,
          }
          this.grupoVehiculoService.crearVariosGrupoVehiculoNuevo(this.nuevaListaGrupoVehiculo).subscribe(lgv => {

          });

          this.grupoService.actualizarGrupo(this.idGrupoActual, grupo).subscribe(res => {
            this.router.navigate(["/grupo/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Editado con éxito');
          });

        } else {

          let grupo: Grupo = {
            id: 0,
            nombre: this.grupoFormulario!.value.nombre,
            descripcion: this.grupoFormulario!.value.descripcion,
          }
          this.grupoService.crearGrupoNuevo(grupo).subscribe(res => {
            this.router.navigate(["/grupo/", res.id]);
            this.cargarFormulario(res.id);
            this.inactivo = true;
            this.toastr.success('Creado con éxito');
          });
        }
      }
    });
  }

  actualizar() {


    this.inactivo = false;
    if (!this.inactivo) {

      this.grupoFormulario?.get('nombre')?.enable();
      this.grupoFormulario?.get('descripcion')?.enable();
    }
    this.cargarFormulario(this.idGrupoActual);

    this.cargarGrupoVehiculo();
  }

  volver() {
    if (!this.inactivo) {
      let modalRef = this.modalService.open(ModalAlertaComponent);
      modalRef.componentInstance.mensaje = "¿Estás seguro de que quieres volver atras?";
      modalRef.componentInstance.mensaje2 = "¡Se perderán los cambios no guardados!";
      modalRef.componentInstance.respuesta.subscribe((res: boolean) => {
        if (res) {
          this.router.navigate(["/grupo"]);
          this.toastr.warning("Saliendo sin guardar")
        }
      });
    } else {
      this.router.navigate(["/grupo"]);
    }


  }

  campoNoEsValido(campo: string) {
    return this.grupoFormulario!.controls?.[campo].errors
      && this.grupoFormulario!.controls?.[campo].touched
  }

  meterVehiculoAlGrupo() {
    let listadoVehiculosDisponibles: Vehiculo[] = [];
    let vehiculosAIncluir: Vehiculo[] = [];

    let modal = this.modalService.open(GrupoModalComponent);
    // this.cargarGrupoVehiculo();
    this.vehiculoService.leerTodosLosVehiculos(0, 50).subscribe(v => {
      listadoVehiculosDisponibles = v;
      modal.componentInstance.vehiculosDisponibles = listadoVehiculosDisponibles;
      modal.componentInstance.emisorVehiculo.subscribe((r: Vehiculo) => {

        if (this.listadoDeVehiculos.filter(val => val.id == r.id).length == 0) {
          this.listadoDeVehiculos.push(r);
          vehiculosAIncluir.push(r);
        }

        vehiculosAIncluir.forEach(v => {
          let grupoVehiculoAGuardar: GrupoVehiculo = {
            id: 0,
            grupo: this.grupoActual!,
            vehiculo: v
          }

          this.nuevaListaGrupoVehiculo.push(grupoVehiculoAGuardar);

          console.log(this.nuevaListaGrupoVehiculo);
        })
      });

    });
  }

  eliminarVehiculoDeGrupo(vehiculo: Vehiculo) {

    let pos = this.nuevaListaGrupoVehiculo.findIndex(v => v.vehiculo.id == vehiculo.id);

    let paraEliminar = this.nuevaListaGrupoVehiculo[pos];

    if (paraEliminar.id > 0) {
      this.grupoVehiculoService.eliminarUnGrupoVehiculo(paraEliminar.id).subscribe();
    }

    this.nuevaListaGrupoVehiculo.splice(pos, 1);
    this.listadoDeVehiculos.splice(pos, 1)
  }

}
