import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-alerta',
  templateUrl: './modal-alerta.component.html',
  styleUrls: ['./modal-alerta.component.css']
})
export class ModalAlertaComponent implements OnInit {

  @Input() mensaje?: string;
  @Input() mensaje2?: string;
  @Output() respuesta: EventEmitter<boolean> = new EventEmitter();

  constructor(public activeModal: NgbActiveModal) {}
  
  ngOnInit(): void {
    
  }

  confirmado(){
    this.respuesta.emit(true);
    this.activeModal.close();
  }

}
