import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VehiculoMaestroComponent } from './components/vehiculo/vehiculo-maestro/vehiculo-maestro.component';
import { VehiculoDetalleComponent } from './components/vehiculo/vehiculo-detalle/vehiculo-detalle.component';
import { GrupoMaestroComponent } from './components/grupo/grupo-maestro/grupo-maestro.component';
import { GrupoDetalleComponent } from './components/grupo/grupo-detalle/grupo-detalle.component';
import { TipoMaestroComponent } from './components/tipo/tipo-maestro/tipo-maestro.component';
import { TipoDetalleComponent } from './components/tipo/tipo-detalle/tipo-detalle.component';
import { MarcaMaestroComponent } from './components/marca/marca-maestro/marca-maestro.component';
import { MarcaDetalleComponent } from './components/marca/marca-detalle/marca-detalle.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './components/home/home/home.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GrupoModalComponent } from './components/grupo/grupo-modal/grupo-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalAlertaComponent } from './components/modales/modal-alerta/modal-alerta.component';
import { ToastrModule } from 'ngx-toastr';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    VehiculoMaestroComponent,
    VehiculoDetalleComponent,
    GrupoMaestroComponent,
    GrupoDetalleComponent,
    TipoMaestroComponent,
    TipoDetalleComponent,
    MarcaMaestroComponent,
    MarcaDetalleComponent,
    HomeComponent,
    GrupoModalComponent,
    ModalAlertaComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
