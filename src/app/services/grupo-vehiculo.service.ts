import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Grupo } from '../models/grupo';
import { GrupoVehiculo } from '../models/grupovehiculo';
import { Vehiculo } from '../models/vehiculo';

@Injectable({
  providedIn: 'root'
})
export class GrupoVehiculoService {

  URL_API: string = environment.baseUrl + '/grupovehiculo';

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  constructor(private http: HttpClient) { }

  crearGrupoVehiculoNuevo(grupovehiculo: GrupoVehiculo): Observable<GrupoVehiculo>{
    return this.http.post<GrupoVehiculo>(this.URL_API, grupovehiculo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Añadido GrupoVehiculo nueva con id=${grupovehiculo.id}`)), 
      catchError(this.handleError<GrupoVehiculo>('crearGrupoVehiculoNuevo')),
    );
  }

  crearVariosGrupoVehiculoNuevo(grupovehiculo: GrupoVehiculo[]): Observable<GrupoVehiculo[]>{
    return this.http.post<GrupoVehiculo[]>(`${this.URL_API}/crearmultiple`, grupovehiculo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Añadido lista GrupoVehiculo nueva`)), 
      catchError(this.handleError<GrupoVehiculo[]>('crearVariosGrupoVehiculoNuevo')),
    );
  }

  leerNumeroDePaginas(size:number): Observable<any>{
    return this.http.get<number>(`${this.URL_API}/paginas?size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos cantidad de paginas`)),
      catchError(this.handleError<GrupoVehiculo[]>('leerNumeroDePaginas')),
    );
  }

  leerVehiculosDeUnGrupo(page:number, size:number, id:number): Observable<Vehiculo[]>{
    
    return this.http.get<Vehiculo[]>(`${this.URL_API}/grupo?page=${page}&size=${size}&grupo=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo Vehiculos de un grupo`)),
      catchError(this.handleError<Vehiculo[]>('leerVehiculosDeUnGrupo')),
    );
  }

  leerGruposDeUnVehiculo(page:number, size:number, id:number): Observable<Grupo[]>{
    
    return this.http.get<Grupo[]>(`${this.URL_API}/vehiculo?page=${page}&size=${size}&vehiculo=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo Grupos de un Vehiculo`)),
      catchError(this.handleError<Grupo[]>('leerGruposDeUnVehiculo')),
    );
  }

  leerTodosLosGrupoVehiculos(page:number, size:number): Observable<GrupoVehiculo[]>{
    return this.http.get<GrupoVehiculo[]>(`${this.URL_API}?page=${page}&size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos GrupoVehiculos`)),
      catchError(this.handleError<GrupoVehiculo[]>('leerTodosLosGrupoVehiculos')),
    );
  }

  leerGrupoVehiculoPorId(id: number): Observable<GrupoVehiculo>{
    return this.http.get<GrupoVehiculo>(`${this.URL_API}/id?id=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo GrupoVehiculo con id=${id}`)),
      catchError(this.handleError<GrupoVehiculo>('leerGrupoVehiculoPorId')),
    );
  }

  leerGruposVehiculosPorGrupo(grupoId: number): Observable<GrupoVehiculo[]>{
    
    return this.http.get<GrupoVehiculo[]>(`${this.URL_API}/listapor/grupo?grupo=${grupoId}`)
    .pipe(
      tap(_ => console.log(`Leyendo GruposVehiculos de grupo con id=${grupoId}`)),
      catchError(this.handleError<GrupoVehiculo[]>('leerGruposVehiculosPorGrupo')),
    );
  }

  actualizarGrupoVehiculo(id:number, GrupoVehiculoNuevo: GrupoVehiculo): Observable<GrupoVehiculo>{
    return this.http.put<GrupoVehiculo>(`${this.URL_API}/modificar?id=${id}`, GrupoVehiculoNuevo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Actualizando GrupoVehiculo con id=${id}`)),
      catchError(this.handleError<GrupoVehiculo>('actualizarGrupoVehiculo')),
    );
    
  }

  eliminarUnGrupoVehiculo(id: number): Observable<GrupoVehiculo>{
    return this.http.delete<GrupoVehiculo>(`${this.URL_API}/eliminar?id=${id}`)
    .pipe(
      tap(_ => console.log(`Eliminado GrupoVehiculo con id=${id}`)),
      catchError(this.handleError<GrupoVehiculo>('delete')),
    );
  }

  eliminarUnaListaGrupoVehiculo(lista: GrupoVehiculo[]): Observable<GrupoVehiculo[]>{
    return this.http.put<GrupoVehiculo[]>(`${this.URL_API}/eliminarvarios`, lista, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Eliminado lista GrupoVehiculo`)),
      catchError(this.handleError<GrupoVehiculo[]>('delete')),
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
