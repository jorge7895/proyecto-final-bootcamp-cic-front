import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Marca } from '../models/marca';

@Injectable({
  providedIn: 'root'
})
export class MarcaService {

  URL_API: string = environment.baseUrl + '/marca';

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  constructor(private http: HttpClient) { }

  crearMarcaNuevo(marca: Marca): Observable<Marca>{
    return this.http.post<Marca>(this.URL_API, marca, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Añadido Marca nueva con id=${marca.id}`)), 
      catchError(this.handleError<Marca>('crearMarcaNuevo')),
    );
  }

  leerNumeroDePaginas(size:number): Observable<any>{
    return this.http.get<number>(`${this.URL_API}/paginas?size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos cantidad de paginas`)),
      catchError(this.handleError<Marca[]>('leerNumeroDePaginas')),
    );
  }

  leerTodosLosMarcas(page:number, size:number): Observable<Marca[]>{
    return this.http.get<Marca[]>(`${this.URL_API}?page=${page}&size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos Marcas`)),
      catchError(this.handleError<Marca[]>('leerTodosLosMarcas')),
    );
  }

  leerMarcaPorId(id: number): Observable<Marca>{
    return this.http.get<Marca>(`${this.URL_API}/id?id=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo Marca con id=${id}`)),
      catchError(this.handleError<Marca>('leerMarcaPorId')),
    );
  }

  leerMarcaPorNombre(nombre: string): Observable<Marca>{
    return this.http.get<Marca>(`${this.URL_API}/nombre?nombre=${nombre}`)
    .pipe(
      tap(_ => console.log(`Leyendo Marca con nombre=${nombre}`)),
      catchError(this.handleError<Marca>('leerMarcaPorNombre')),
    );
  }

  actualizarMarca(id:number, MarcaNuevo: Marca): Observable<Marca>{
    return this.http.put<Marca>(`${this.URL_API}/modificar?id=${id}`, MarcaNuevo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Actualizando Marca con id=${id}`)),
      catchError(this.handleError<Marca>('actualizarMarca')),
    );
    
  }

  eliminarUnMarca(id: number): Observable<Marca>{
    return this.http.delete<Marca>(`${this.URL_API}/eliminar?id=${id}`)
    .pipe(
      tap(_ => console.log(`Eliminado Marca con id=${id}`)),
      catchError(this.handleError<Marca>('delete')),
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
