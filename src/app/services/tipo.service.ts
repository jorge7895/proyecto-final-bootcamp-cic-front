import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tipo } from '../models/tipo';

@Injectable({
  providedIn: 'root'
})
export class TipoService {

  URL_API: string = environment.baseUrl + '/tipo';

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  constructor(private http: HttpClient) { }

  crearTipoNuevo(tipo: Tipo): Observable<Tipo>{
    return this.http.post<Tipo>(this.URL_API, tipo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Añadido tipo nuevo con id=${tipo.id}`)), 
      catchError(this.handleError<Tipo>('crearTipoNuevo')),
    );
  }

  leerNumeroDePaginas(size:number): Observable<any>{
    return this.http.get<number>(`${this.URL_API}/paginas?size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos cantidad de paginas`)),
      catchError(this.handleError<Tipo[]>('leerNumeroDePaginas')),
    );
  }

  leerTodosLosTipos(page:number, size:number): Observable<Tipo[]>{
    return this.http.get<Tipo[]>(`${this.URL_API}?page=${page}&size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos tipos`)),
      catchError(this.handleError<Tipo[]>('leerTodosLosTipos')),
    );
  }

  leerTipoPorId(id: number): Observable<Tipo>{
    return this.http.get<Tipo>(`${this.URL_API}/id?id=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo Tipo con id=${id}`)),
      catchError(this.handleError<Tipo>('leerTipoPorId')),
    );
  }

  leerTipoPorNombre(nombre: string): Observable<Tipo>{
    return this.http.get<Tipo>(`${this.URL_API}/nombre?nombre=${nombre}`)
    .pipe(
      tap(_ => console.log(`Leyendo Tipo con nombre=${nombre}`)),
      catchError(this.handleError<Tipo>('leerTipoPorNombre')),
    );
  }

  actualizarTipo(id:number, tipoNuevo: Tipo): Observable<Tipo>{
    return this.http.put<Tipo>(`${this.URL_API}/modificar?id=${id}`, tipoNuevo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Actualizado tipo con id=${id}`)),
      catchError(this.handleError<Tipo>('actualizarTipo')),
    );
    
  }

  eliminarUnTipo(id: number): Observable<Tipo>{
    return this.http.delete<Tipo>(`${this.URL_API}/eliminar?id=${id}`)
    .pipe(
      tap(_ => console.log(`Eliminado Tipo con id=${id}`)),
      catchError(this.handleError<Tipo>('delete')),
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
