import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Grupo } from '../models/grupo';

@Injectable({
  providedIn: 'root'
})
export class GrupoService {

  URL_API: string = environment.baseUrl + '/grupo';

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  constructor(private http: HttpClient) { }

  crearGrupoNuevo(grupo: Grupo): Observable<Grupo>{
    return this.http.post<Grupo>(this.URL_API, grupo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Añadido Grupo nueva con id=${grupo.id}`)), 
      catchError(this.handleError<Grupo>('crearGrupoNuevo')),
    );
  }

  leerNumeroDePaginas(size:number): Observable<any>{
    return this.http.get<number>(`${this.URL_API}/paginas?size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos cantidad de paginas`)),
      catchError(this.handleError<Grupo[]>('leerNumeroDePaginas')),
    );
  }

  leerTodosLosGrupos(page:number, size:number): Observable<Grupo[]>{
    return this.http.get<Grupo[]>(`${this.URL_API}?page=${page}&size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos Grupos`)),
      catchError(this.handleError<Grupo[]>('leerTodosLosGrupos')),
    );
  }

  leerGrupoPorId(id: number): Observable<Grupo>{
    return this.http.get<Grupo>(`${this.URL_API}/id?id=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo Grupo con id=${id}`)),
      catchError(this.handleError<Grupo>('leerGrupoPorId')),
    );
  }

  leerGrupoPorNombre(nombre: string): Observable<Grupo>{
    return this.http.get<Grupo>(`${this.URL_API}/nombre?nombre=${nombre}`)
    .pipe(
      tap(_ => console.log(`Leyendo Grupo con nombre=${nombre}`)),
      catchError(this.handleError<Grupo>('leerGrupoPorNombre')),
    );
  }

  actualizarGrupo(id:number, GrupoNuevo: Grupo): Observable<Grupo>{
    return this.http.put<Grupo>(`${this.URL_API}/modificar?id=${id}`, GrupoNuevo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Actualizando Grupo con id=${id}`)),
      catchError(this.handleError<Grupo>('actualizarGrupo')),
    );
    
  }

  eliminarUnGrupo(id: number): Observable<Grupo>{
    return this.http.delete<Grupo>(`${this.URL_API}/eliminar?id=${id}`)
    .pipe(
      tap(_ => console.log(`Eliminado Grupo con id=${id}`)),
      catchError(this.handleError<Grupo>('delete')),
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
