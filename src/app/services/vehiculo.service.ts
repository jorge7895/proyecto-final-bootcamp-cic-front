import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable, tap, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Vehiculo } from '../models/vehiculo';

@Injectable({
  providedIn: 'root'
})
export class VehiculoService {

  URL_API: string = environment.baseUrl + '/vehiculo';

  httpOptions = {headers: new HttpHeaders({ 'Content-Type': 'application/json' })};

  constructor(private http: HttpClient, 
    private toastr: ToastrService) { }

  crearVehiculoNuevo(vehiculo: Vehiculo): Observable<Vehiculo>{
    return this.http.post<Vehiculo>(this.URL_API, vehiculo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Añadido vehiculo nuevo con id=${vehiculo.id}`)), 
      catchError(this.handleError<Vehiculo>('crearVehiculoNuevo')),
    );
  }

  leerNumeroDePaginas(size:number): Observable<any>{
    return this.http.get<number>(`${this.URL_API}/paginas?size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos cantidad de paginas`)),
      catchError(this.handleError<number>('leerNumeroDePaginas')),
    );
  }

  leerTodosLosVehiculos(page:number, size:number): Observable<Vehiculo[]>{
    return this.http.get<Vehiculo[]>(`${this.URL_API}?page=${page}&size=${size}`)
    .pipe(
      tap(_ => console.log(`Leyendos vehiculos`)),
      catchError(this.handleError<Vehiculo[]>('leerTodosLosVehiculos')),
    );
  }

  leerVehiculoPorId(id: number): Observable<Vehiculo>{
    return this.http.get<Vehiculo>(`${this.URL_API}/id?id=${id}`)
    .pipe(
      tap(_ => console.log(`Leyendo Vehiculo con id=${id}`)),
      catchError(this.handleError<Vehiculo>('leerVehiculoPorId')),
    );
  }

  leerVehiculoPorNombre(nombre: string): Observable<Vehiculo>{
    return this.http.get<Vehiculo>(`${this.URL_API}/nombre?nombre=${nombre}`)
    .pipe(
      tap(_ => console.log(`Leyendo Vehiculo con nombre=${nombre}`)),
      catchError(this.handleError<Vehiculo>('leerVehiculoPorNombre')),
    );
  }

  leerVehiculoPorTipo(page:number, size:number, tipo: string): Observable<Vehiculo[]>{
    return this.http.get<Vehiculo[]>(`${this.URL_API}/tipo?page=${page}&size=${size}&tipo=${tipo}`)
    .pipe(
      tap(_ => console.log(`Leyendo Vehiculo con tipo=${tipo}`)),
      catchError(this.handleError<Vehiculo[]>('leerVehiculoPorTipo')),
    );
  }

  leerVehiculoPorMarca(page:number, size:number, marca: string): Observable<Vehiculo[]>{
    return this.http.get<Vehiculo[]>(`${this.URL_API}/marca?page=${page}&size=${size}&marca=${marca}`)
    .pipe(
      tap(_ => console.log(`Leyendo Vehiculos con marca=${marca}`)),
      catchError(this.handleError<Vehiculo[]>('leerVehiculoPorMarca')),
    );
  }

  actualizarVehiculo(id:number, vehiculoNuevo: Vehiculo): Observable<Vehiculo>{
    return this.http.put<Vehiculo>(`${this.URL_API}/modificar?id=${id}`, vehiculoNuevo, this.httpOptions)
    .pipe(
      tap(_ => console.log(`Actualizado vehiculo con id=${id}`)),
      catchError(this.handleError<Vehiculo>('actualizarVehiculo')),
    );
    
  }

  eliminarUnVehiculo(id: number): Observable<Vehiculo>{
    return this.http.delete<Vehiculo>(`${this.URL_API}/eliminar?id=${id}`)
    .pipe(
      tap(_ => console.log(`Eliminado Vehiculo con id=${id}`)),
      catchError(this.handleError<Vehiculo>('delete')),
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      this.toastr.error(`Error en la operación de: ${operation}`)
      return of(result as T);
    };
  }
}
