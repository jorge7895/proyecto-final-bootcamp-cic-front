export interface Marca {

    id: number,
    nombre: string,
	sede: string,
	anoCreacion: number,
	descripcion: string,
}
