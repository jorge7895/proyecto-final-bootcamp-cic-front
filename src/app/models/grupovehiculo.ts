import { Grupo } from "./grupo";
import { Vehiculo } from "./vehiculo";

export interface GrupoVehiculo {

    id: number,
    vehiculo: Vehiculo,
	grupo: Grupo,
}