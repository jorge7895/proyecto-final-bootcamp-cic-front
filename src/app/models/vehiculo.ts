import { Marca } from "./marca";
import { Tipo } from "./tipo";
import { TipoCombustible } from "./tipo-combustible";
import { TipoTraccion } from "./tipo-traccion";

export interface Vehiculo {

    id: number,
    idTipo: number,
	tipo: Tipo | undefined,
    idMarca: number | string,
	marca: Marca | undefined,
    modelo: string,
    matricula: string,
	kilometros: number,
	anoFabricacion: number,
	descripcion: string,
	puertas: number,
	plazas: number,
	traccion: TipoTraccion,
	combustible: TipoCombustible,
}
