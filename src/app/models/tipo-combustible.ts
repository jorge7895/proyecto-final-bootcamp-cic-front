export enum TipoCombustible {
	DIESEL = 'Diesel',
	GASOLINA = 'Gasolina',
	HIBRIDO = 'Hibrido',
	ELECTRICO = 'Electrico',
	PEDALES = 'Pedales'
}
