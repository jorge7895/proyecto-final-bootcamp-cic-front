import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GrupoDetalleComponent } from './components/grupo/grupo-detalle/grupo-detalle.component';
import { GrupoMaestroComponent } from './components/grupo/grupo-maestro/grupo-maestro.component';
import { HomeComponent } from './components/home/home/home.component';
import { MarcaDetalleComponent } from './components/marca/marca-detalle/marca-detalle.component';
import { MarcaMaestroComponent } from './components/marca/marca-maestro/marca-maestro.component';
import { TipoDetalleComponent } from './components/tipo/tipo-detalle/tipo-detalle.component';
import { TipoMaestroComponent } from './components/tipo/tipo-maestro/tipo-maestro.component';
import { VehiculoDetalleComponent } from './components/vehiculo/vehiculo-detalle/vehiculo-detalle.component';
import { VehiculoMaestroComponent } from './components/vehiculo/vehiculo-maestro/vehiculo-maestro.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'vehiculo', component: VehiculoMaestroComponent },
  { path: 'vehiculo/:id', component: VehiculoDetalleComponent },
  { path: 'tipo', component: TipoMaestroComponent },
  { path: 'tipo/:id', component: TipoDetalleComponent },
  { path: 'marca', component: MarcaMaestroComponent },
  { path: 'marca/:id', component: MarcaDetalleComponent },
  { path: 'grupo', component: GrupoMaestroComponent },
  { path: 'grupo/:id', component: GrupoDetalleComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
